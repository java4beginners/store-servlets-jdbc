package ua.com.java4beginners.model;

public class Product {
    private Integer id;
    private String title;
    private Integer price;

    public Product(Integer id, String title, Integer price) {
        this.id = id;
        this.title = title;
        this.price = price;
    }

    public Product(String title, Integer price) {
        this.title = title;
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Integer getPrice() {
        return price;
    }
}
