package ua.com.java4beginners.jdbc;

import ua.com.java4beginners.model.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductDao {

    public Product findById(Integer productId) {
        try {
            Connection conn = getConnection();
            PreparedStatement ps = conn.prepareStatement("select id, title, price from products where id = ?");
            ps.setInt(1, productId);
            ResultSet rs = ps.executeQuery();
            Product product = null;
            if (rs.next()) {
                product = new Product(
                        rs.getInt("id"),
                        rs.getString("title"),
                        rs.getInt("price")
                );
                conn.close();
            }
            return product;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    static {
        try {
            DriverManager.getConnection(
                    "jdbc:h2:file:./data/store;INIT=RUNSCRIPT FROM 'classpath:scripts/create.sql'",
                    "sa", "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection(
                "jdbc:h2:file:./data/store;DB_CLOSE_ON_EXIT=FALSE'",
                "sa", "");
    }

    public List<Product> findAll() {
        try {
            ArrayList<Product> products = new ArrayList<>();
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("select id, title, price from products");
            while (rs.next()) {
                Product product = new Product(
                        rs.getInt("id"),
                        rs.getString("title"),
                        rs.getInt("price")
                );
                products.add(product);
            }
            conn.close();
            return products;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void save(Product product) {
        try {
            Connection conn = getConnection();
            PreparedStatement ps = conn.prepareStatement("insert into products(title, price) values(?, ?)");
            ps.setString(1, product.getTitle());
            ps.setInt(2, product.getPrice());
            ps.execute();
            conn.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
