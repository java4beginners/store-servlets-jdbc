package ua.com.java4beginners.servlets;

import ua.com.java4beginners.jdbc.ProductDao;
import ua.com.java4beginners.model.Product;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(loadOnStartup = 1, urlPatterns = "/cart/products")
public class CartServlet extends HttpServlet {


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Integer productId = Integer.valueOf(req.getParameter("id"));
        putProductIntoSession(req, productId);
        resp.sendRedirect("/products");
    }

    private void putProductIntoSession(HttpServletRequest req, Integer productId) {
        HttpSession session = req.getSession(true);
        if (session.getAttribute("cart") == null) {
            session.setAttribute("cart", new ArrayList<Product>());
        }

        List<Product> cart = (List<Product>) session.getAttribute("cart");
        Product product = new ProductDao().findById(productId);
        if (product != null) {
            cart.add(product);
        }
    }
}
