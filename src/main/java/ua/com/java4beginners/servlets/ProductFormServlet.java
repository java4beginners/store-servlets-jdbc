package ua.com.java4beginners.servlets;

import ua.com.java4beginners.jdbc.ProductDao;
import ua.com.java4beginners.model.Product;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(loadOnStartup = 1, urlPatterns = "/products/form")
public class ProductFormServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        PrintWriter out = resp.getWriter();
        out.write("<html><head>" +
                "<link rel='stylesheet' href='/webjars/bootstrap/4.4.1/css/bootstrap.min.css'>" +
                "</head>" +
                "<body>" +
                "<div class='container'>" +
                "   <h1 class='alert alert-primary'>Create New Product</h1>" +
                "   <form class='form' action='/products/form' method='POST'>" +
                "       <label>Title</label>" +
                "       <input class='form-control' type='text' name='title'>" +
                "       <label>Price</label>" +
                "       <input class='form-control' type='number' name='price'>" +
                "       <input type='submit' value='Save' class='btn btn-primary'>" +
                "   </form>" +
                "</div>" +
                "</body>" +
                "</html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        ProductDao productDao = new ProductDao();
        Product product = new Product(
                req.getParameter("title"),
                Integer.valueOf(req.getParameter("price")));
        productDao.save(product);
        resp.sendRedirect("/products");
    }
}
