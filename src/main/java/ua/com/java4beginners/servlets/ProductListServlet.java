package ua.com.java4beginners.servlets;

import org.apache.commons.text.StringSubstitutor;
import ua.com.java4beginners.jdbc.ProductDao;
import ua.com.java4beginners.model.Product;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@WebServlet(loadOnStartup = 1, urlPatterns = "/products")
public class ProductListServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        List<Product> products = new ProductDao().findAll();
        generatePageWithProducts(req, resp, products);
    }

    private void generatePageWithProducts(HttpServletRequest req, HttpServletResponse resp, List<Product> products) throws IOException {
        PrintWriter out = resp.getWriter();
        out.write("<html><head>" +
                "<link rel='stylesheet' href='/webjars/bootstrap/4.4.1/css/bootstrap.min.css'>" +
                "</head>" +
                "<body>" +
                "<div class='container'>" +
                "<div class='jumbotron'>" +
                "   <h1>Products</h1>" +
                "<a href='/products/form'>Add Product</a>" +
                "</div>");
        writeProductList(products, out);

        List<Product> cart = getProductsInCart(req);
        out.write("<h3 class='alert alert-primary'>Cart</h3>");
        writeProductList(cart, out);


        out.write("</div></body></html>");
    }

    private List<Product> getProductsInCart(HttpServletRequest req) {
        List<Product>  cart = (List<Product>) req.getSession(true).getAttribute("cart");
        return cart == null ? new ArrayList<>() : cart;
    }

    private void writeProductList(List<Product> products, PrintWriter out) {
        out.write("<ul class='list-unstyled'>");
        for (Product product : products) {
            String html = writeProduct(product);
            out.write(html);
        }
        out.write("</ul>");
    }

    private String writeProduct(Product product) {
        HashMap<String, Object> dynamicData = inMap(product);

        String template =
                "<li>${title}, ${price} " +
                "   <form class='d-inline-block' method='POST' action='/cart/products'>" +
                "       <input type='hidden' name='id' value='${id}'>" +
                "       <button type='submit' class='btn btn-primary'>Add</button>" +
                "   </form>" +
                "</li>";
        return StringSubstitutor.replace(template, dynamicData);
    }

    private HashMap<String, Object> inMap(Product product) {
        HashMap<String, Object> dynamicData = new HashMap<>();
        dynamicData.put("id", product.getId());
        dynamicData.put("price", product.getPrice());
        dynamicData.put("title", product.getTitle());
        return dynamicData;
    }


}
