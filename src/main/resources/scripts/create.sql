create table IF NOT EXISTS products(id integer auto_increment, title varchar(255), price integer);
delete from products;
insert into products(title, price) values('Note Book', 1200);
insert into products(title, price) values('Couch', 800);
insert into products(title, price) values('Piano', 2300);